;*********************************************************************
;Direct color DMA test with built in image converter.
;Direct color DMA code by Chilly Willy
;Image converter and additional code by ComradeOj
;Contact: ComradeOj@yahoo.com
;*********************************************************************  
	include "ramdat.asm"
	include "header.asm"
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss         ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
 		move.w #$100,($A11100)
		move.w #$100,($A11200) 
		move.l #$00000000,sp
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram	
		
		move.b #$01,screen_ID	
		lea (font),a5
		move.w #$7FF,d4
		move.l #$40000000,(a3)
		bsr vram_loop
		
		move.l #$c0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)

		lea (music4)+284,a2 ;skip YM2612 register loading
		move.l a2,vgm_start		
		lea (music4)+64,a2
		
		lea (titletext),a5
		move.l #$6A800000,(a3) ;$2A80
		bsr termtextloop
	
		lea (title_end),a5		;The ass end of the BMP. Why are these things backwards?
		lea ram_start,a0 	;genesis ram start
		move.w #$7FA8,d4
		bsr convert_image	
		move.w #$8F00,(a3)		;auto increment	of zero
		clr d0	
		move.w #$2300,sr
loop:
		move.w #$8174,(a3)	;V-INT, DMA enable, screen enable
		move.l	#$40000000, (A3)	
vbwait1:
		btst.b	#3, $1(A3)
		beq	vbwait1
vbwait2:
		btst.b	#3, $1(A3)
		bne	vbwait2
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4) ;Clear FIFO
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		nop
		nop
		nop
		nop			
		move.l	#$93f7947f, (A3) ;DMA length ($7Ff7)
		move.l	#$95009600, (A3) ;using long fixed the jitter issue for some reason...
		move.l	#$977f8134, (A3) 
		move.l	#$C0000080, (A3) ;Blast processing time!
		move.w #$2700,sr
		move.w #$8174,(a3)	;V-INT, DMA enable, screen enable		
		bsr music_driver	;Runs between the end of the the DMA area and Vblank.
		move.w #$2300,sr
		bra loop

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter	
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts 
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts 
		
convert_image:
        move.b -(a5),d5  ;first byte
        move.b -(a5),d0  ;second byte
	lsl.w #$08,d5	 ;position it!
	add.w d5,d0		 ;smash them together. (Can't use .W, because of odd numbered address)
	move.w d0,d1	 ;save a copy of the data in d1 and d2
	move.w d0,d2
	andi.w #$7C00,d0 ;get red channel
	andi.w #$03E0,d1 ;green
	andi.w #$001F,d2 ;blue
	lsr.w #$08,d0	 ;get correct value (11 shifts for red)
	lsr.w #$03,d0
	;andi.w #$0E,d0	  ;Not needed? Extra bit seems to be ignored by VDP.
	lsr.w #$06,d1
	;andi.w #$0E,d1   ;Keeping these enabled just to be safe.	
	lsr.w #$01,d2
	;andi.w #$0E,d2		
	lsl.w #$08,d2	 ;shift color bits into correct places.
	lsl.w #$04,d1	
	eor.w d0,d2		 ;add red to finished product
	eor.w d1,d2		 ;add green
        move.w d2,(a0)+  ;write finished product in to memory.
        dbf d4,convert_image
        rts  	
		
clear_regs:
		moveq #$00000000,d0
		moveq #$00000000,d1
		moveq #$00000000,d2
		moveq #$00000000,d3
		moveq #$00000000,d4
		moveq #$00000000,d5
		moveq #$00000000,d6
		moveq #$00000000,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		lea ram_start,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts	
		
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts	

termtextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra termtextloop		
		
HBlank:
        rte
return:
        rts
VBlank:
		add.w #$01,vblanks
		bsr game
        rte			
game:
		move.b #$00,VB_flag
		cmpi.b #$ff,killflag
		beq dead
		bsr read_controller
		bsr check_input
		rts
dead:
		bsr read_controller
		cmpi.b #$7f,d3
		beq start
		rts
		
check_input:		;different set of inputs for different screens
		lea (inputs),a1
		moveq #$00000000,d1
		move.b screen_ID,d1
        lsl.b #$1,d1		    ;locate the correct table
		sub.w #$02,d1           ;step back a word 
		add.w d1,a1             ;adjust the address register 
		move.w (a1),d1
		move.l d1,a1            ;switch to direct addressing 
		jmp (a1)	
		
inputs:
 dc.w input_title
 dc.w input1
 dc.w input2
 dc.w input3
 dc.w input_cont
 dc.w input4	;wire
 dc.w input_cont
 dc.w input5	;scar
 dc.w input_cont
 dc.w input_end ;win
 
input_title:
		cmpi.b #$7f,d3
		beq newimage
		rts 
input_cont:
		cmpi.b #$7f,d3
		beq newimage
		rts

input1:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7 ;a
		beq death1
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7 ;c
		beq death1
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7 ;b
		beq newimage
		rts 
input2:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7 ;a
		beq newimage
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7 ;c
		beq death3
		move.b d3,d7
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7 ;b
		beq death3
		move.b d3,d7
		rts
input3:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7 ;a
		beq death2
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7 ;B
		beq death2
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7 ;c
		beq newimage
		rts 
input4:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7 ;a
		beq death4
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7 ;B
		beq newimage
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7 ;c
		beq death5
		rts
input5:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7 ;a
		beq newimage
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7 ;B
		beq death6
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7 ;c
		beq death6
		rts
input_end:
		cmpi.b #$7f,d3
		beq start
		rts 		
		
newimage:
		add.b #$01,screen_ID
		lea (screens),a1
		moveq #$00000000,d1
		move.b screen_ID,d1
        lsl.b #$1,d1		    ;locate the correct table
		sub.w #$02,d1           ;step back a word 
		add.w d1,a1             ;adjust the address register 
		move.w (a1),d1
		move.l d1,a1            ;switch to direct addressing 
		jmp (a1)
		
screens:
 dc.w return  ;null (title)
 dc.w screen1 ;walk direction
 dc.w screen2 ;color wires
 dc.w screen3 ;mystic
 dc.w screen4 ;continue
 dc.w screen5 ;wire
 dc.w screen6 ;continue
 dc.w screen7 ;math
 dc.w screen8 ;victory
 dc.w screen9 ;end
 
 
screen1:
		lea (music1)+284,a2 ;skip YM2612 register loading
		move.l a2,vgm_start		
		lea (music1)+64,a2	
		lea (text1),a5
		move.w #$8F02,(a3)		;fix auto increment		
		move.l #$6A800000,(a3)  ;VRAM $2A80
		bsr termtextloop
		move.w #$8F00,(a3)		;return auto increment		
		lea (image1_end),a5
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image			
screen2:
		lea (text2),a5
		move.w #$8F02,(a3)	
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (image2_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen3:
		lea (music3)+200,a2
		move.l a2,vgm_start		
		lea (music3)+64,a2	
		lea (text3),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (image3_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen4:
		lea (music2)+200,a2
		move.l a2,vgm_start		
		lea (music2)+64,a2	
		lea (cont1),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (image4_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen5:
		lea (wiretext),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (wire_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen6:
		lea (cont2),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (crawl_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen7:
		lea (music7)+200,a2
		move.l a2,vgm_start		
		lea (music7)+64,a2	
		lea (text5),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (scar_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen8:
		lea (wintext),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (win_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
screen9:
		lea (music8)+64,a2
		move.l a2,vgm_start		
		lea (music8)+64,a2	
		lea (endtext),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (ending_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
death1:
		move.b #$ff,killflag
		lea (music5)+200,a2
		move.l a2,vgm_start		
		lea (music5)+64,a2	
		lea (firstdeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (death1_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image	
death2:
		move.b #$ff,killflag
		lea (music6)+200,a2
		move.l a2,vgm_start		
		lea (music6)+64,a2	
		lea (pigdeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (death2_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
death3:
		move.b #$ff,killflag
		lea (music5)+200,a2
		move.l a2,vgm_start		
		lea (music5)+64,a2	
		lea (slipdeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (death1_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image
death4:
		move.b #$ff,killflag
		lea (music5)+200,a2
		move.l a2,vgm_start		
		lea (music5)+64,a2	
		lea (jumpdeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (death1_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image	
death5:
		move.b #$ff,killflag
		lea (music5)+200,a2
		move.l a2,vgm_start		
		lea (music5)+64,a2	
		lea (planedeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (plane_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image	
death6:
		move.b #$ff,killflag
		lea (music5)+200,a2
		move.l a2,vgm_start		
		lea (music5)+64,a2	
		lea (scardeath),a5
		move.w #$8F02,(a3)
		move.l #$6A800000,(a3)
		bsr termtextloop
		move.w #$8F00,(a3)
		lea (scarfall_end),a5	
		lea ram_start,a0
		move.w #$7FA8,d4
		bra convert_image			

read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1		
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0			
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	

hang:
		bra hang
		
unhold:
		bsr read_controller
		cmpi.b #$ff,d3
		 beq return
		bra unhold
		
	include "music_driver_V2.asm"
	include "crashscreen.asm"
	include "data.asm"
ROM_End:                    