VDPSetupArray:
	dc.w $8014		
	dc.w $8154  ;Genesis mode, DMA enabled, VBLANK-INT disabled		
	dc.w $8208	;field A    
	dc.w $8300	;$833e	
	dc.w $8401	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8Aff  ;no H-INT	
	dc.w $8B00		
	dc.w $8C81	;40 wide mode, no S+H
	dc.w $8D34		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
text1:
 dc.b " You begin your adventure.              "
 dc.b " What do you do next?                   "
 dc.b "                                        "
 dc.b " A= Walk left                           "
 dc.b " B= Walk forward                        "
 dc.b " C= Give up                             "
 dc.b "                                       %"
 
text2:
 dc.b " You reach the end of the desk.         "
 dc.b " Which wire do you climb?               "
 dc.b "                                        "
 dc.b " A= Red wire                            "
 dc.b " B= Green wire                          "
 dc.b " C= Blue wire%"
 
text3:
 dc.b " A mysterious person blocks your way!   "
 dc.b " They don't look too friendly. How do   "
 dc.b " you get past?                          "
 dc.b " A= Use your gun                        "
 dc.b " B= Try to run past them                "
 dc.b " C= Lightning hands%"
 
text4:
 dc.b " It's a pretty long way across to the   "
 dc.b " next shelf. How will you progress?     "
 dc.b "                                        "
 dc.b " A= Swing across with the rope          "
 dc.b " B= Use the paper airplane              "
 dc.b " C= Use the jet pack% "
 
text5:
 dc.b " I'm terrible, so you must do math in   "
 dc.b " order to progress! Or you can guess.   " 
 dc.b " 5(3x+2)-2=-2(1-7x) Solve for X.        "
 dc.b " A= -10                                 "	;correct
 dc.b " B=  10                                 "
 dc.b " C=  14                                %"
 
wiretext:
 dc.b " You make it to the end of the shelf.   "
 dc.b " Your goal is in sight! But how to get  "
 dc.b " there, you wonder.                     "
 dc.b " A= Think happy thoughts and jump       "
 dc.b " B= Crawl across the wire bridge        "
 dc.b " C= Ride the paper airplane across     %" 
 
cont1:
 dc.b " With a bang and a flash the mystic     "
 dc.b " offender is turned into a smoldering   "
 dc.b " pile of ash.                           "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "        Press START to continue         "
 dc.b "                                       %"
 
titletext:
 dc.b "      The Adventures of DMA David       "
 dc.b "   Episode 1: The quest for The Ooze.   "
 dc.b "         Press START to begin!          "
 dc.b "                                        "
 dc.b " Code, photos, and music by ComradeOj.  "
 dc.b " Core color DMA code by Chilly Willy.   "
 dc.b "          Visit www.mode5.net          %"
 
firstdeath:
 dc.b " Dead on the first one? Oh come on!     "
 dc.b " Look on the bright side, you won't be  "
 dc.b " sent back very far!                    "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
 
slipdeath 
 dc.b " Well, you had a one in three chance.   "
 dc.b " The odds were stacked against you!     "
 dc.b " Hopefully you choose the right one     "
 dc.b " next time round!                       "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
 
jumpdeath 
 dc.b " Oh come on! Did you really think that  "
 dc.b " was going to work?                     "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
 
planedeath:
 dc.b " So close, yet so far...                "
 dc.b " I guess that paper airplane wasn't     "
 dc.b " made very well, and couldn't glide     "
 dc.b " too far.                               "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
scardeath:
 dc.b " Either you're not very good at math,   "
 dc.b " or I'm not. Either way, one of us is   "
 dc.b " wrong. Either way, you are about to    "
 dc.b " have a long fall with a quick stop.    "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
 
 
pigdeath: 
 ; dc.b " Little did you know, that woman is the " 
 ; dc.b " infamous mystic offender. The only     "
 ; dc.b " things that can defeat her are quick   "
 ; dc.b " pigs, and electrical arcs.             "
 ; dc.b "                                        "
 ; dc.b "        Press START to try again!%"
 dc.b " Bang! Zap! Little did you know, that   "
 dc.b " woman is the infamous mystic offender. "
 dc.b " In an instant you are transformed into "
 dc.b " a pig 'All too easy', she says.        "
 dc.b "                                        "
 dc.b "        Press START to try again!%"
 
endtext:
 dc.b "            Congratulations!            "
 dc.b " You have conquered everything, all in  "
 dc.b " the name of reaching your favorite     "
 dc.b " Genesis game! Thanks for playing!      "
 dc.b "                                        "
 dc.b "       Press START to play again.  %"
 
wintext:
 dc.b " You made it! Now it's time to pop the  "
 dc.b " game in, power up your Genesis, sit    "
 dc.b " back, and enjoy The Ooze.              "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "        Press START to continue         "
 dc.b "                                       %"
 
cont2:
 dc.b " It looks like you'll make it across    "
 dc.b " slowly, but surely! Don't look down!   "
 dc.b " As you crawl closer to your prize, you "
 dc.b " see a lion walk around it from behind. "
 dc.b " 'Oh boy, this can't be good.'          "
 dc.b "        Press START to continue         "
 dc.b "                                       %"
 
music1:
	incbin "music/music1.vgm"	;start-mystic
music2:
	incbin "music/music2.vgm"	;wire-
music3:
	incbin "music/music3.vgm"	;mystic offender
music4:
	incbin "music/music4.vgm"	;title
music5:
	incbin "music/music5.vgm"	;not crazy train
music6:
	incbin "music/music6.vgm"	;pig death
music7:
	incbin "music/music7.vgm"	;drum thing
music8:
 	incbin "music/dr. caine.vgm"	;ending
font:
	incbin "img/ascii.bin"
	incbin "img/hexascii.bin"

	incbin "img/img1v2.bmp"
image1_end:
	incbin "img/img2v2.bmp"
image2_end:
	incbin "img/img3v2.bmp"
image3_end:
	incbin "img/img4.bmp"
image4_end:
	incbin "img/death1.bmp"
death1_end:
	incbin "img/death2.bmp"
death2_end:
	incbin "img/title.bmp"
title_end:
	incbin "img/wire.bmp"
wire_end:
	incbin "img/crawl.bmp"
crawl_end:
	incbin "img/plane.bmp"
plane_end:	
	incbin "img/scar.bmp"
scar_end:
	incbin "img/end.bmp"
ending_end:
	incbin "img/win.bmp"
win_end:
	incbin "img/scarfall.bmp"
scarfall_end:
