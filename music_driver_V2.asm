music_driver:
vgm_loop:		
		;rts ;for testing
		clr d6
		bsr test2612
		
        move.b (a2)+,d6
		cmpi.b #$61,d6
		 beq wait		
	
		 cmpi.b #$66,d6
		 beq loop_playback 
		
		cmpi.b #$52,d6 
		 beq update2612_0
		cmpi.b #$53,d6 
		 beq update2612_1
		cmpi.b #$50,d6
		 beq update_psg
		bra vgm_loop
	
update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra vgm_loop
		
update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra vgm_loop
		
loop_playback:
 		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011		
		move.l vgm_start,a2
		rts
		;bra vgm_loop
update_psg:
        move.b (a2)+,$C00011
		bra vgm_loop
	
wait:
		add.l #$02,a2
		rts
		
test2612:
		clr d6
        move.b $A04001,d6
		andi.b #$80,d6
        cmpi.b #$80,d6
		beq test2612 
		rts			
